# docker-tor

Browse Privately. Explore Freely. In Docker.

```
docker pull wrzlbrmft/tor:latest
```

```
docker run -p 9050:9050 --network host wrzlbrmft/tor:latest
```

After configuring your browser, open https://check.torproject.org/ .

See also:

  * https://www.torproject.org/
  * https://hub.docker.com/r/wrzlbrmft/tor/tags

## License

The content of this repository is distributed under the terms of the
[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html).
