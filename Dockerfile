FROM alpine:latest

EXPOSE 9050 9051

ENTRYPOINT [ "/usr/bin/tor", "-f", "/etc/tor/torrc" ]

RUN apk upgrade --no-cache \
    && apk add --no-cache \
        tor

COPY torrc /etc/tor
